# How to run

## Requirements

* java 1.7.x
* maven 3 (to run tests)

In order to run the program you need pass an argument with a csv file (comma delimited) with the input data in the
following format:

    fisrtName,lastName,salary,superRate,paymentStartDate

Then, to execute just type:

    java -cp myob-coding-exercise-1.0-SNAPSHOT.jar com.myob.app.App [CSV FILE]

Example

    java -cp myob-coding-exercise-1.0-SNAPSHOT.jar com.myob.app.App /tmp/input.csv

output

    David,Rudd,01 March – 31 March,5004,922,4082,450
    Ryan,Chen,01 March – 31 March,10000,2696,7304,1000
    Fabian,Cancelara,01 March – 31 March,2083,108,1975,208
    Nairo,Quintana,01 March – 31 March,1500,0,1500,150
    Peter,Sagan,01 March – 31 March,16667,5296,11371,1667

If you do not pass the csv file argument a help will show up

    java -cp myob-coding-exercise-1.0-SNAPSHOT.jar com.myob.app.App
    
    Usage: java -cp [PATH TO JAR FILE] com.myob.app.App [INPUT FILE IN CSV FORMART]
    
    CSV file format:
    fisrtName,lastName,salary,superRate,paymentStartDate



### CSV content sample

    David,Rudd,60050,9%,01 March – 31 March
    Ryan,Chen,120000,10%,01 March – 31 March
    Ryan,Chen,120000,10%
    Ryan,Chen,AAA,10%
    Fabian,Cancelara,-120000,10%,01 March – 31 March
    Fabian,Cancelara,25000,10%,01 March – 31 March
    Nairo,Quintana,18000,10%,01 March – 31 March
    Peter,Sagan,200000,10%,01 March – 31 March

# Assumptions

* The program has a very simple error handling. It just checks if the salary is a positive integer and if superRate 
is between 0 and 50.
* The csv must be comma delimited
* Only well formatted lines are processed
* SuperRate in the csv file must be in the format: 10% (number, followed by % sign)
* The output is written to StdIn

# Tests

Some unit tests were written for each salary range provided in the tax table. All tests are in the EmployeeTest.java file.


        this.employee_1 = new Employee("David", "Rudd", 60050, 9, "01 March – 31 March");
        this.employee_2 = new Employee("Ryan", "Chen", 120000, 10, "01 March – 31 March");
        this.employee_3 = new Employee("Nairo", "Quintana", 18000, 10, "01 March – 31 March");
        this.employee_4 = new Employee("Fabian", "Cancelara", 25000, 10, "01 March – 31 March");
        this.employee_5 = new Employee("Peter", "Sagan", 200000, 10, "01 March – 31 March");

## Running the tests

    git clone https://bitbucket.org/gustavosoares/myob-coding-exercise.git
    cd myob-coding-exercise
    mvn compile
    
    [INFO] Scanning for projects...
    [INFO]                                                                         
    [INFO] ------------------------------------------------------------------------
    [INFO] Building myob-coding-exercise 1.0-SNAPSHOT
    [INFO] ------------------------------------------------------------------------
    [INFO] 
    [INFO] --- maven-resources-plugin:2.3:resources (default-resources) @ myob-coding-exercise ---
    [WARNING] Using platform encoding (UTF-8 actually) to copy filtered resources, i.e. build is platform dependent!
    [INFO] skip non existing resourceDirectory /home/gustavo/repos/git/bitbucket/myob-coding-exercise/myob-coding-exercise/src/main/resources
    [INFO] 
    [INFO] --- maven-compiler-plugin:3.3:compile (default-compile) @ myob-coding-exercise ---
    [INFO] Nothing to compile - all classes are up to date
    [INFO] ------------------------------------------------------------------------
    [INFO] BUILD SUCCESS
    [INFO] ------------------------------------------------------------------------
    [INFO] Total time: 1.302s
    [INFO] Finished at: Thu Sep 10 21:12:06 BRT 2015
    [INFO] Final Memory: 7M/150M
    [INFO] ------------------------------------------------------------------------

    mvn test
    
    -------------------------------------------------------
     T E S T S
    -------------------------------------------------------
    Running com.myob.app.AppTest
    Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.017 sec
    Running com.myob.app.EmployeeTest
    Tests run: 10, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.015 sec
    
    Results :
    
    Tests run: 11, Failures: 0, Errors: 0, Skipped: 0
    
    [INFO] ------------------------------------------------------------------------
    [INFO] BUILD SUCCESS
    [INFO] ------------------------------------------------------------------------
    [INFO] Total time: 1.842s
    [INFO] Finished at: Thu Sep 10 21:14:34 BRT 2015
    [INFO] Final Memory: 9M/213M
    [INFO] ------------------------------------------------------------------------
