package com.myob.app;

import junit.framework.TestCase;

/**
 * Created by gustavo on 09/09/15.
 */
public class EmployeeTest extends TestCase {

    private Employee employee_1, employee_2, employee_3, employee_4, employee_5;

    public void setUp() throws Exception {
        super.setUp();

        this.employee_1 = new Employee("David", "Rudd", 60050, 9, "01 March – 31 March");
        this.employee_2 = new Employee("Ryan", "Chen", 120000, 10, "01 March – 31 March");
        this.employee_3 = new Employee("Nairo", "Quintana", 18000, 10, "01 March – 31 March");
        this.employee_4 = new Employee("Fabian", "Cancelara", 25000, 10, "01 March – 31 March");
        this.employee_5 = new Employee("Peter", "Sagan", 200000, 10, "01 March – 31 March");
    }

    public void tearDown() throws Exception {
        super.tearDown();
        this.employee_1 = null;
        this.employee_2 = null;
        this.employee_3 = null;
        this.employee_4 = null;
        this.employee_5 = null;
    }

    public void testGetFistName() throws Exception {
        assertEquals("David", this.employee_1.getFirstName());
    }

    public void testSetFistName() throws Exception {
        this.employee_1.setFirstName("Ryan");
        assertEquals("Ryan", this.employee_1.getFirstName());
    }

    public void testGetLastName() throws Exception {
        assertEquals("Rudd", this.employee_1.getLastName());
    }

    public void testSetLastName() throws Exception {
        this.employee_1.setLastName("Chen");
        assertEquals("Chen", this.employee_1.getLastName());
    }

    public void testGetAnualSalary() throws Exception {
        assertEquals(60050, this.employee_1.getAnualSalary());
    }

    public void testSetAnualSalary() throws Exception {
        this.employee_1.setAnualSalary(70000);
        assertEquals(70000, this.employee_1.getAnualSalary());
    }

    /**
     * salary / 12
     * @throws Exception
     */
    public void testComputeGrossIncome() throws Exception {
        assertEquals(5004, this.employee_1.getGrossIncome());
        assertEquals(10000, this.employee_2.getGrossIncome());
        assertEquals(1500, this.employee_3.getGrossIncome());
        assertEquals(2083, this.employee_4.getGrossIncome());
        assertEquals(16667, this.employee_5.getGrossIncome());
    }

    /**
     * income tax = based on the tax table provide below
     * income tax = (3,572 + (60,050 - 37,000) x 0.325) / 12  = 921.9375 (round up) = 922
     * @throws Exception
     */
    public void testComputeIncomeTax() throws Exception {
        assertEquals(922, this.employee_1.getIncomeTax());
        assertEquals(2696, this.employee_2.getIncomeTax());
        assertEquals(0, this.employee_3.getIncomeTax());
        assertEquals(108, this.employee_4.getIncomeTax());
        assertEquals(5296, this.employee_5.getIncomeTax());
    }

    /**
     * net income = gross income - income tax
     * net income = 5,004 - 922 = 4,082
     * @throws Exception
     */
    public void testComputeNetIncome() throws Exception {
        assertEquals(4082, this.employee_1.getNetIncome());
        assertEquals(7304, this.employee_2.getNetIncome());
        assertEquals(1500, this.employee_3.getNetIncome());
        assertEquals(1975, this.employee_4.getNetIncome());
        assertEquals(11371, this.employee_5.getNetIncome());

    }

    /**
     * super = gross income x super rate
     * super = 5,004 x 9% = 450.36 (round down) = 450
     * @throws Exception
     */
    public void testComputeSuper() throws Exception {
        assertEquals(450, this.employee_1.getSuper());
        assertEquals(1000, this.employee_2.getSuper());
        assertEquals(150, this.employee_3.getSuper());
        assertEquals(208, this.employee_4.getSuper());
        assertEquals(1667, this.employee_5.getSuper());
    }

}