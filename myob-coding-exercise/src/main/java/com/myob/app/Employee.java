package com.myob.app;

import java.text.DecimalFormat;

/**
 * Created by gustavo on 09/09/15.
 */
public class Employee {

    private String firstName;
    private String lastName;
    private int anualSalary;
    private double superRate;
    private String paymentStartDate;

    private int grossIncome = 0;
    private int incomeTax = 0;
    private int netIncome = 0;

    public Employee(String firstName, String lastName, int anualSalary, double superRate, String paymentStartDate) {
        this.firstName = firstName;
        this.lastName = lastName;

        if (anualSalary < 0) throw new NumberFormatException("Salary should be positive");
        if (superRate < 0.0 || superRate > 50.0) throw new NumberFormatException("super rate should be between 0 and 50");
        this.anualSalary = anualSalary;
        this.superRate = superRate;
        this.paymentStartDate = paymentStartDate;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAnualSalary() {
        return this.anualSalary;
    }

    public void setAnualSalary(int anualSalary) {
        this.anualSalary = anualSalary;
    }

    public double getSuperRate() {
        return this.superRate;
    }

    public void setSuperRate(double superRate) {
        this.superRate = superRate;
    }

    public String getPaymentStartDate() {
        return this.paymentStartDate;
    }

    private void computeGrossIncome() {
        this.grossIncome = this.doRound(this.anualSalary / 12.0);
    }

    public int getGrossIncome() {
        this.computeGrossIncome();
        return this.grossIncome;
    }

    private int doRound(double number) {
        DecimalFormat df = new DecimalFormat("####0.000");
        String mediator = df.format(number);
        mediator = mediator.substring(mediator.indexOf('.') + 1);
        Integer result = Integer.valueOf(mediator);
        if (result >= 500) {
            // roundUp
            return (int) Math.ceil(number);
        } else {
            // roundDown
            return (int) Math.floor(number);
        }
    }

    private void computeIncomeTax() {
        /***
         * income tax = (3,572 + (60,050 - 37,000) x 0.325) / 12  = 921.9375 (round up) = 922
         */

        double cents = 0.0;
        int initial = 0;
        int over = 0;
                ;
        if (this.anualSalary >= 0 && this.anualSalary <= 18200) {

        } else if (this.anualSalary > 18200 && this.anualSalary <= 37000) {
            cents = 0.190;
            over = 18200;
        } else if (this.anualSalary > 37000 && this.anualSalary <= 80000) {
            initial = 3572;
            cents = 0.325;
            over = 37000;
        } else if (this.anualSalary > 80000 && this.anualSalary <= 180000) {
            initial = 17547;
            cents = 0.370;
            over = 80000;
        } else if (this.anualSalary > 180000) {
            initial = 54547;
            cents = 0.450;
            over = 180000;
        }

        //income tax = (3,572 + (60,050 - 37,000) x 0.325) / 12  = 921.9375 (round up) = 922
        double aux = new Double((this.anualSalary - over) * cents);
        this.incomeTax = this.doRound((initial + (int) aux) / 12.0);

    }

    public int getIncomeTax() {
        this.computeIncomeTax();
        return this.incomeTax;
    }

    private void computeNetIncome() {
        int grossIncome = this.getGrossIncome();
        int incomeTax = this.getIncomeTax();
        this.netIncome = grossIncome - incomeTax;
    }

    public int getNetIncome() {
        this.computeNetIncome();
        return this.netIncome;
    }

    public int getSuper() {
        double aux = this.getGrossIncome() * (this.superRate / 100.0);
        return this.doRound(aux);
    }
}
