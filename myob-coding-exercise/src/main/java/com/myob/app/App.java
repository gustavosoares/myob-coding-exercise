package com.myob.app;

import java.io.File;
import java.util.*;

/**
 * Hello world!
 *
 */
public class App 
{

    private static void help() {
        System.out.println();
        System.out.println("Usage: java -cp [PATH TO JAR FILE] com.myob.app.App [INPUT FILE IN CSV FORMART]");
        System.out.println();
        System.out.println("CSV file format:");
        System.out.println("fisrtName,lastName,salary,superRate,paymentStartDate");
        System.out.println();
        System.out.println();

        System.exit(1);
    }

    public static List<Employee> processInput(String inputFileName) {

        List<Employee> employees = new ArrayList<Employee>();

        // Open the file
        Scanner fileScanner = null;
        try {
            File file = new File(inputFileName);
            fileScanner = new Scanner(file);

            //read file
            while (fileScanner.hasNextLine()) {
                String strLine = fileScanner.nextLine();
                StringTokenizer st = new StringTokenizer(strLine,",");
                if (st.countTokens() != 5) {
                    //System.out.println("invalid line -> "+strLine);
                    continue;
                }
                while (st.hasMoreTokens()) {
                    String firstName = st.nextToken().trim();
                    String lastName = st.nextToken().trim();
                    try {
                        int salary = Integer.parseInt(st.nextToken().trim());
                        String superRate_str = st.nextToken().trim();
                        double superRate = Double.parseDouble(superRate_str.substring(0, superRate_str.indexOf('%')));
                        String paymentStartDate = st.nextToken().trim();
                        employees.add(new Employee(firstName,lastName,salary,superRate,paymentStartDate));
                    } catch (NumberFormatException e) {
                        //System.out.println("ops... invalid line -> "+strLine);
                    }

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e);
        } finally {
            fileScanner.close();
        }

        return employees;
    }

    public static void main( String[] args )
    {

        if (args.length == 0) help();
        String inputFileName = args[0];

        List<Employee> employees = processInput(inputFileName);

        for (Employee employee: employees ) {
            System.out.format("%s,%s,%s,%d,%d,%d,%d%n", employee.getFirstName(),
                                                        employee.getLastName(),
                                                        employee.getPaymentStartDate(),
                                                        employee.getGrossIncome(),
                                                        employee.getIncomeTax(),
                                                        employee.getNetIncome(),
                                                        employee.getSuper());
        }

    }
}
